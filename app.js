(function(){
	var todos = [];
	document.querySelector('#submit-task').addEventListener('click', function(){
		var todosList = document.querySelector('#todos-list'),
			todoRow = document.createElement('tr'),
			todoName = document.getElementById('input-name').value,
			todoDate = document.getElementById('input-date').value,
			todoId = todos.length+1;

		todoRow.id = 'todo-'+todoId;
		todoRow.innerHTML = `<td>${todoName}</td>
							<td>${todoDate}</td>
							<td><a href="#" class="btn btn-danger" data-id="${todoId}">Delete</a></td>`;
		todosList.append(todoRow);

		document.getElementById('input-name').value = '';
		document.getElementById('input-date').value = '';

	});
})();